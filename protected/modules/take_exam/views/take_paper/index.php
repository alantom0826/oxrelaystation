<?php
/* @var $this DefaultController */

$this->breadcrumbs=array(
	$this->module->id,
);
?>
<style>
textaresa{/* Text Area 固定大小*/
 max-width:250px;
 max-height:100px;
 width:250px;
 height:100px;
 margin: 0px;
}
</style>
<h1><?php //echo $this->uniqueId . '/' . $this->action->id; ?></h1>
<h1>自動出卷</h1>

<form>
  <div class="row">
    <div class="large-4 columns">
      <label>試卷名稱
        <input type="text" placeholder="例如:100學年度數學第一次平時考" />
      </label>
    </div>
    <div class="large-4 columns">
      <label>試卷標題
        <input type="text" placeholder="※此項欄位資料會直接顯示於試卷上面" />
      </label>
    </div>
    <div class="large-4 columns">
      <div class="row collapse">
        <label>出卷型式</label>
        <div class="small-9 columns">
            <select>
                <option value="paper">紙本</option>
                <option value="mutil">多媒體</option>
            </select>
        </div>
        <div class="small-3 columns">
          <span class="postfix">型式</span>
        </div>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="large-12 columns">
      <label>試卷描述
        <textarea name="comments" style="max-width:100%;max-height:100px;width:100%;height:100px;"></textarea>
      </label>
    </div>
  </div>
  <div class="row">
    <div class="large-4 columns">
      <label>教育程度
        <select>
          <option value="1">國小</option>
          <option value="2">國中</option>
          <option value="3">高中</option>
          <option value="4">大學 </option>
        </select>
      </label>
    </div>
    <div class="large-4 columns">
      <label>科目
        <select>
          <option value="1">英文</option>
          <option value="2">數學</option>
        </select>
      </label>
    </div>
    <div class="large-4 columns">
      <label>題庫來源
        <select>
          <option value="1">台北市</option>
          <option value="2">新北市</option>
          <option value="3">高雄市</option>
          <option value="4">其它</option>
        </select>
      </label>
    </div>
  </div>
  <div class="row">
    <div class="large-6 columns">
      <label>Choose Your Favorite</label>
      <input type="radio" name="pokemon" value="Red" id="pokemonRed"><label for="pokemonRed">Red</label>
      <input type="radio" name="pokemon" value="Blue" id="pokemonBlue"><label for="pokemonBlue">Blue</label>
    </div>
    <div class="large-6 columns">
      <label>Check these out</label>
      <input id="checkbox1" type="checkbox"><label for="checkbox1">Checkbox 1</label>
      <input id="checkbox2" type="checkbox"><label for="checkbox2">Checkbox 2</label>
    </div>
  </div>
  <div class="row">
    <div class="large-12 columns">
      <label>Textarea Label
        <textarea placeholder="small-12.columns"></textarea>
      </label>
    </div>
  </div>
</form>

