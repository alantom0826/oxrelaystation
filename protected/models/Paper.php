<?php

/**
 * This is the model class for table "papers".
 *
 * The followings are the available columns in table 'papers':
 * @property integer $num_of_times
 * @property string $start_at
 * @property string $name
 * @property integer $mysql_paper_id
 * @property integer $mysql_exam_id
 * @property integer $subject_id
 * @property string $end_at
 * @property string $parts
 * @property integer $duration
 * @property string $desc
 * @property integer $expire
 */
class Paper extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'papers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('num_of_times, start_at, name, mysql_paper_id, mysql_exam_id, subject_id, parts, duration, desc, expire', 'required'),
            array('num_of_times, start_at, name, mysql_paper_id, mysql_exam_id, parts, duration', 'required'),
			array('num_of_times, mysql_paper_id, mysql_exam_id, subject_id, duration, expire', 'numerical', 'integerOnly'=>true),
			array('name, desc', 'length', 'max'=>255),
			array('end_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('num_of_times, start_at, name, mysql_paper_id, mysql_exam_id, subject_id, end_at, parts, duration, desc, expire', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'num_of_times' => 'Num Of Times',
			'start_at' => 'Start At',
			'name' => 'Name',
			'mysql_paper_id' => 'Mysql Paper',
			'mysql_exam_id' => 'Mysql Exam',
			'subject_id' => 'Subject',
			'end_at' => 'End At',
			'parts' => 'Parts',
			'duration' => 'Duration',
			'desc' => 'Desc',
			'expire' => 'Expire',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('num_of_times',$this->num_of_times);
		$criteria->compare('start_at',$this->start_at,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('mysql_paper_id',$this->mysql_paper_id);
		$criteria->compare('mysql_exam_id',$this->mysql_exam_id);
		$criteria->compare('subject_id',$this->subject_id);
		$criteria->compare('end_at',$this->end_at,true);
		$criteria->compare('parts',$this->parts,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('expire',$this->expire);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Paper the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
