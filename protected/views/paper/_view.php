<?php
/* @var $this PaperController */
/* @var $data Paper */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('mysql_exam_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->mysql_exam_id), array('view', 'id'=>$data->mysql_exam_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num_of_times')); ?>:</b>
	<?php echo CHtml::encode($data->num_of_times); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_at')); ?>:</b>
	<?php echo CHtml::encode($data->start_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mysql_paper_id')); ?>:</b>
	<?php echo CHtml::encode($data->mysql_paper_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_at')); ?>:</b>
	<?php echo CHtml::encode($data->end_at); ?>
	<br />

    <b><?php /*echo CHtml::encode($data->getAttributeLabel('parts')); ?>:</b>
	<?php echo CHtml::encode($data->parts); ?>
	<br />
    */?>

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('duration')); ?>:</b>
	<?php echo CHtml::encode($data->duration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('desc')); ?>:</b>
	<?php echo CHtml::encode($data->desc); ?>
	<br />

	*/ ?>

</div>
