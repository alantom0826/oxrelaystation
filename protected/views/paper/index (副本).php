<?php
$json = '{ "num_of_times" : "1", "start_at" : "2014-01-15 00:00:00", "name" : "test02", "mysql_paper_id" : "9", "mysql_exam_id" : "11", "end_at" : "2014-01-16 00:00:00", "parts" : [ [ { "name" : "藥卷測試", "questions" : [ { "topic" : "A:What&#039;s his name? B:{{q}}", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "He is not Jack.", "Yes, he is Jack.", "His name is Jack.", "Yes, his name is Jack." ], "mysql_q_id" : "5", "score" : "10.00", "ans" : [ "C" ] }, { "topic" : "A:Who is the girl? B:{{q}}", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "She is a nurse(護士).", "She is nine.", "She is new.", "She is my sister." ], "mysql_q_id" : "6", "score" : "10.00", "ans" : [ "D" ] }, { "topic" : "A:How old is Jane&#039;s sister? B:{{q}}", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "He is nine years old.", "She is one years old.", "She is three years old.", "They are twelve years old." ], "mysql_q_id" : "8", "score" : "10.00", "ans" : [ "C" ] }, { "topic" : "A:{{q}} is in the classroom? B:Mary is.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "Where", "What", "How", "Who" ], "mysql_q_id" : "18", "score" : "10.00", "ans" : [ "D" ] }, { "topic" : "A:{{q}} is Mr. Wang ?&amp;lt;br/&amp;gt;B: He is my father.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "Why", "Who", "What", "Where" ], "mysql_q_id" : "670", "score" : "10.00", "ans" : [ "B" ] }, { "topic" : "He {{q}} a lot of fun at the magic club.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "has", "have", "is", "are" ], "mysql_q_id" : "697", "score" : "10.00", "ans" : [ "A" ] }, { "topic" : "He {{q}} a lot of fun at the magic club.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "has", "have", "is", "are" ], "mysql_q_id" : "707", "score" : "10.00", "ans" : [ "A" ] }, { "topic" : "A: Is Superman ever afraid of bad men?&amp;lt;br/&amp;gt;B:No, he {{q}}", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "never is", "is never", "never does", "does never" ], "mysql_q_id" : "753", "score" : "10.00", "ans" : [ "A" ] }, { "topic" : "History {{q}} itself. Sometimes humans make the same mistakes again and again.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "repeats", "reads", "excuses", "changes" ], "mysql_q_id" : "1219", "score" : "10.00", "ans" : [ "A" ] }, { "topic" : "Jim: How old {{q}} you, Carol?&amp;lt;br/&amp;gt;Carol: I am 14 years old.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "are", "is", "am", "be" ], "mysql_q_id" : "1513", "score" : "10.00", "ans" : [ "A" ] } ], "desc" : "藥卷測試" } ] ], "duration" : "60", "desc" : "" }';
$json = str_replace("{{q}}","____",$json);
$json = str_replace("&amp;lt;","<",str_replace("&amp;gt;",">",$json));

//~ $data = file_get_contents("php://input");
$data = json_decode($json); 

//~ echo "name = " .$data->name. "<br>";
//~ echo "name = " .$data->parts[0][0]->name. "<br>";
//~ echo "topic = " .$data->parts[0][0]->questions[0]->topic. "<br>";
//~ echo "opt_ans[0] = " .$data->parts[0][0]->questions[0]->opt_ans[0]. "<br>";
//~ echo "opt_ans_desc[0] = " .$data->parts[0][0]->questions[0]->opt_ans_desc[0]. "<br>";
//~ echo "mysql_q_id = " .$data->parts[0][0]->questions[0]->mysql_q_id. "<br>";
//~ echo "score = " .$data->parts[0][0]->questions[0]->score. "<br>";
//~ echo "ans = " .$data->parts[0][0]->questions[0]->ans[0]. "<br>";
//~ 
//~ echo "opt_ans num = " .count($data->parts[0][0]->questions[0]->opt_ans). "<br>";
//~ echo "questions num = " .count($data->parts[0][0]->questions). "<br>";
//~ 
//~ 
//~ foreach ($data->parts[0][0]->questions[0]->opt_ans as $i) {
    //~ echo $i. "<br>";
//~ }
//~ 
//~ 
//~ 
//~ 
//~ foreach ($data->parts[0][0]->questions[0]->opt_ans_desc as $i) {
    //~ echo $i. "<br>";
//~ }

//~ $arr = array("one", "two", "three");
//~ reset($arr);
//~ while (list($key, $value) = each($data->parts[0][0]->questions[0]->opt_ans_desc)) {
    //~ echo "Keya: $key; Value: $value<br />\n";
//~ }

//~ foreach ($data->parts[0][0]->questions[0]->opt_ans_desc as $key => $value) {
    //~ echo "Keyb: $key; Value: $value<br />\n";
//~ }
//~ 
//~ echo "<br />";echo "<br />";echo "<br />";echo "<br />";
?>
<div class="row">
    <div class="large-12 columns">
<form name="form1" method="post" action="http://127.0.0.1:8080/result">
<?php
for ($q=0; $q < count($data->parts[0][0]->questions); $q++){
    echo "<br />";
    $q_num = $q + 1;
    echo "<h1>". $q_num . ". " .$data->parts[0][0]->questions[$q]->topic. "</h1><br>";
    
    for ($i=0; $i < count($data->parts[0][0]->questions[$q]->opt_ans); $i++){
        $opt_ans = $data->parts[0][0]->questions[$q]->opt_ans[$i];
        echo "<input type=\"radio\" name=\"group$q\" value=$opt_ans>";
        echo $data->parts[0][0]->questions[$q]->opt_ans[$i];
        echo ". ";
        echo $data->parts[0][0]->questions[$q]->opt_ans_desc[$i]. "<br>";
    }
}
?>
<input type="submit" value="送出計算">
<input type="reset" value="重設">
</form>
</div>
</div>



