<?php
/* @var $this PaperController */
/* @var $model Paper */

$this->breadcrumbs=array(
	'Papers'=>array('index'),
	$model->name=>array('view','id'=>$model->mysql_exam_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Paper', 'url'=>array('index')),
	array('label'=>'Create Paper', 'url'=>array('create')),
	array('label'=>'View Paper', 'url'=>array('view', 'id'=>$model->mysql_exam_id)),
	array('label'=>'Manage Paper', 'url'=>array('admin')),
);
?>

<h1>Update Paper <?php echo $model->mysql_exam_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>