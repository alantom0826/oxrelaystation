<script type="text/javascript"
  src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>

<?php
/* @var $this PaperController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Papers',
);

$this->menu=array(
	array('label'=>'Create Paper', 'url'=>array('create')),
	array('label'=>'Manage Paper', 'url'=>array('admin')),
);
?>

<?php
Yii::import('application.controllers.PaperController');
$json = file_get_contents("php://input");

// receive json and save to file
//~ echo   $_SERVER['DOCUMENT_ROOT'];
//~ echo "<br />";
//~ echo   __FILE__;
//~ $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "myText.txt","a+");
//~ fwrite($fp,$json);
//~ fclose($fp);

if (isset($json)){
    $data = json_decode($json);
    //~ $json = str_replace("{{q}}","____",$json);
    //~ $json = str_replace("&amp;lt;","<",str_replace("&amp;gt;",">",$json));
    //~ $data = json_decode($json);
    
    if (isset($data)){
        $chkjson = array_key_exists('parts', $data);
        if ($chkjson){
            $parts = json_encode($data->parts, JSON_UNESCAPED_UNICODE);
            $data = json_encode($data, JSON_UNESCAPED_UNICODE);
            $data = json_decode($data);
            
            $mysql_exam_id = $data->mysql_exam_id;  
            $num_of_times = $data->num_of_times;  
            $start_at = $data->start_at;
            $name = $data->name;
            $mysql_paper_id = $data->mysql_paper_id;
            $subject_id = $data->subject_id;
            $end_at = $data->end_at;
            $duration = $data->duration;
            $desc = $data->desc;

            $checkuser = Users::model()->findByPk($mysql_exam_id);
            if (isset($checkuser)){
                $model4update=$this->loadModel($mysql_exam_id);
                $model = $model4update;
            }else{
                $model=new Paper;
            }

            $model->mysql_exam_id = $mysql_exam_id;
            $model->subject_id = $subject_id;
            $model->num_of_times = $num_of_times;  
            $model->start_at = $start_at;
            $model->name = $name;
            $model->mysql_paper_id = $mysql_paper_id;
            $model->end_at = $end_at;
            $model->parts = $parts;
            $model->duration = $duration;
            $model->desc = $desc;

            $model->save();

        }else{
            $mysql_exam_id = $data->mysql_exam_id;
            $model=$this->loadModel($mysql_exam_id);
            $model->delete();
        }
    }
}
?>

<h1>Papers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
