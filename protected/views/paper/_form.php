<?php
/* @var $this PaperController */
/* @var $model Paper */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'paper-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'num_of_times'); ?>
		<?php echo $form->textField($model,'num_of_times'); ?>
		<?php echo $form->error($model,'num_of_times'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_at'); ?>
		<?php echo $form->textField($model,'start_at'); ?>
		<?php echo $form->error($model,'start_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mysql_paper_id'); ?>
		<?php echo $form->textField($model,'mysql_paper_id'); ?>
		<?php echo $form->error($model,'mysql_paper_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mysql_exam_id'); ?>
		<?php echo $form->textField($model,'mysql_exam_id'); ?>
		<?php echo $form->error($model,'mysql_exam_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_at'); ?>
		<?php echo $form->textField($model,'end_at'); ?>
		<?php echo $form->error($model,'end_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'parts'); ?>
		<?php echo $form->textArea($model,'parts',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'parts'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'duration'); ?>
		<?php echo $form->textField($model,'duration'); ?>
		<?php echo $form->error($model,'duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'desc'); ?>
		<?php echo $form->textField($model,'desc',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'desc'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->