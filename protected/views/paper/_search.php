<?php
/* @var $this PaperController */
/* @var $model Paper */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'num_of_times'); ?>
		<?php echo $form->textField($model,'num_of_times'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_at'); ?>
		<?php echo $form->textField($model,'start_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mysql_paper_id'); ?>
		<?php echo $form->textField($model,'mysql_paper_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'mysql_exam_id'); ?>
		<?php echo $form->textField($model,'mysql_exam_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_at'); ?>
		<?php echo $form->textField($model,'end_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parts'); ?>
		<?php echo $form->textArea($model,'parts',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'duration'); ?>
		<?php echo $form->textField($model,'duration'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'desc'); ?>
		<?php echo $form->textField($model,'desc',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->