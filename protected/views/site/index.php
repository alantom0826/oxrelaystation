<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<!--
<p>Congratulations! You have successfully created your Yii application.</p>

<p>You may change the content of this page by modifying the following two files:</p>
<ul>
	<li>View file: <code><?php echo __FILE__; ?></code></li>
	<li>Layout file: <code><?php echo $this->getLayoutFile('main'); ?></code></li>
</ul>
-->
<?php
//~ $cart = array(
  //~ "orderID" => 12345,
  //~ "shopperName" => "John Smith",
  //~ "shopperEmail" => "johnsmith@example.com",
  //~ "contents" => array(
    //~ array(
      //~ "productID" => 34,
      //~ "productName" => "SuperWidget",
      //~ "quantity" => 1
    //~ ),
    //~ array(
      //~ "productID" => 56,
      //~ "productName" => "WonderWidget",
      //~ "quantity" => 3
    //~ )
  //~ ),
  //~ "orderCompleted" => true
//~ );
 //~ 
//~ echo json_encode( $cart);
?>
<p></p>
<?php
//~ $jsonString = '
//~ {                                           
  //~ "orderID": 12345,                         
  //~ "shopperName": "John Smith",              
  //~ "shopperEmail": "johnsmith@example.com",  
  //~ "contents": [                             
    //~ {                                       
      //~ "productID": 34,                      
      //~ "productName": "SuperWidget",         
      //~ "quantity": 1                        
    //~ },                                      
    //~ {                                       
      //~ "productID": 56,                      
      //~ "productName": "WonderWidget",        
      //~ "quantity": 3                         
    //~ }                                       
  //~ ],                                        
  //~ "orderCompleted": true                    
//~ }                                           
//~ ';
 //~ 
//~ $cart = json_decode( $jsonString );
//~ echo $cart->shopperEmail . "";
//~ echo $cart->contents[1]->productName . "";
?>
<p></p>

<?php
//~ $data = '{ "num_of_times" : "1", "start_at" : "2014-01-15 00:00:00", "name" : "test02", "mysql_paper_id" : "9", "mysql_exam_id" : "11", "end_at" : "2014-01-16 00:00:00", "parts" : [ [ { "name" : "藥卷測試", "questions" : [ { "topic" : "A:What&#039;s his name? B:{{q}}", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "He is not Jack.", "Yes, he is Jack.", "His name is Jack.", "Yes, his name is Jack." ], "mysql_q_id" : "5", "score" : "10.00", "ans" : [ "C" ] }, { "topic" : "A:Who is the girl? B:{{q}}", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "She is a nurse(護士).", "She is nine.", "She is new.", "She is my sister." ], "mysql_q_id" : "6", "score" : "10.00", "ans" : [ "D" ] }, { "topic" : "A:How old is Jane&#039;s sister? B:{{q}}", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "He is nine years old.", "She is one years old.", "She is three years old.", "They are twelve years old." ], "mysql_q_id" : "8", "score" : "10.00", "ans" : [ "C" ] }, { "topic" : "A:{{q}} is in the classroom? B:Mary is.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "Where", "What", "How", "Who" ], "mysql_q_id" : "18", "score" : "10.00", "ans" : [ "D" ] }, { "topic" : "A:{{q}} is Mr. Wang ?&amp;lt;br/&amp;gt;B: He is my father.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "Why", "Who", "What", "Where" ], "mysql_q_id" : "670", "score" : "10.00", "ans" : [ "B" ] }, { "topic" : "He {{q}} a lot of fun at the magic club.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "has", "have", "is", "are" ], "mysql_q_id" : "697", "score" : "10.00", "ans" : [ "A" ] }, { "topic" : "He {{q}} a lot of fun at the magic club.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "has", "have", "is", "are" ], "mysql_q_id" : "707", "score" : "10.00", "ans" : [ "A" ] }, { "topic" : "A: Is Superman ever afraid of bad men?&amp;lt;br/&amp;gt;B:No, he {{q}}", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "never is", "is never", "never does", "does never" ], "mysql_q_id" : "753", "score" : "10.00", "ans" : [ "A" ] }, { "topic" : "History {{q}} itself. Sometimes humans make the same mistakes again and again.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "repeats", "reads", "excuses", "changes" ], "mysql_q_id" : "1219", "score" : "10.00", "ans" : [ "A" ] }, { "topic" : "Jim: How old {{q}} you, Carol?&amp;lt;br/&amp;gt;Carol: I am 14 years old.", "opt_ans" : [ "A", "B", "C", "D" ], "opt_ans_desc" : [ "are", "is", "am", "be" ], "mysql_q_id" : "1513", "score" : "10.00", "ans" : [ "A" ] } ], "desc" : "藥卷測試" } ] ], "duration" : "60", "desc" : "" }';
//~ 
//~ //$data = file_get_contents("php://input");
//~ $data = json_decode($data); 
//~ 
//~ echo "name = " .$data->name. "<br>";
//~ echo "name = " .$data->parts[0][0]->name. "<br>";
//~ echo "topic = " .$data->parts[0][0]->questions[0]->topic. "<br>";
//~ echo "opt_ans[0] = " .$data->parts[0][0]->questions[0]->opt_ans[0]. "<br>";
//~ echo "opt_ans_desc[0] = " .$data->parts[0][0]->questions[0]->opt_ans_desc[0]. "<br>";
//~ echo "mysql_q_id = " .$data->parts[0][0]->questions[0]->mysql_q_id. "<br>";
//~ echo "score = " .$data->parts[0][0]->questions[0]->score. "<br>";
//~ echo "ans = " .$data->parts[0][0]->questions[0]->ans[0]. "<br>";

?>

<!--
<p>For more details on how to further develop this application, please read
the <a href="http://www.yiiframework.com/doc/">documentation</a>.
Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
should you have any questions.</p>
-->
