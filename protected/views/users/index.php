<?php
/* @var $this UsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
	array('label'=>'Create Users', 'url'=>array('create')),
	array('label'=>'Manage Users', 'url'=>array('admin')),
);
?>

<?php
Yii::import('application.controllers.UsersController');
Yii::import('application.models.TblProfiles');
$json = file_get_contents("php://input");

// receive json and save to file
//~ echo   $_SERVER['DOCUMENT_ROOT'];
//~ echo "<br />";
//~ echo   __FILE__;
//~ $fp = fopen($_SERVER['DOCUMENT_ROOT'] . "myTextuser.txt","a+");
//~ fwrite($fp,$json);
//~ fclose($fp);

if (isset($json)){
    $data = json_decode($json);
        
    for ($i=0; $i < count($data); $i++){
        
        $exam_info = json_encode($data[$i]->exam_info, JSON_UNESCAPED_UNICODE);
        $class_info = json_encode($data[$i]->class_info, JSON_UNESCAPED_UNICODE);
        
        $data[$i] = json_encode($data[$i], JSON_UNESCAPED_UNICODE);
        $data[$i] = json_decode($data[$i]);
        
        $account = $data[$i]->account;
        $name = $data[$i]->name;
        $mysql_u_id = $data[$i]->mysql_u_id;
        $pwd = $data[$i]->pwd;
        $email = $data[$i]->email;

        $checkuser = Users::model()->findByPk($mysql_u_id);
        if (isset($checkuser)){
            $model4update=$this->loadModel($mysql_u_id);
            $model = $model4update;
            /* diff exam_info */
            if (!empty($model->exam_end)){
                $array1 = $data[$i]->exam_info;
                $array2 = json_decode($model->exam_end);
                $result = array_intersect($array1, $array2);
                for ($j=0; $j<count($result); $j++){
                    //print_r(array_keys($result)[$i]);
                    unset($array1[array_keys($result)[$j]]);
                }
                $array1 = array_values($array1);
                $exam_info = json_encode($array1);
            }
        }else{
            $model=new Users;
        }
        
        $model->mysql_u_id = $mysql_u_id;  
        $model->name = $name;  
        $model->exam_info = $exam_info;
        //$model->account = $account;
        //$model->pwd = $pwd;
        $model->email = $email;
        $model->class_info = $class_info;
        $model->id = $mysql_u_id;
        $model->username = $account;
        $model->password = $pwd;
        $model->activkey = md5(microtime().$account);
        $model->superuser = "0";
        $model->status = "1";

        $model->save();
        
        $checkid = TblProfiles::model()->findByPk($mysql_u_id);
        if (isset($checkid)){
            $pmodel4update=TblProfiles::model()->findByPk($mysql_u_id);
            $pmodel = $pmodel4update;
        }else{
            $pmodel=new TblProfiles;
        }
        
        $pmodel->user_id = $mysql_u_id;
        $pmodel->lastname = $name;
        $pmodel->firstname = $account;
        
        $pmodel->save();

    }
}
?>



<h1>Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

