<?php
/* @var $this UsersController */
/* @var $data Users */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('mysql_u_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->mysql_u_id), array('view', 'id'=>$data->mysql_u_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exam_info')); ?>:</b>
	<?php echo CHtml::encode($data->exam_info); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('account')); ?>:</b>
	<?php echo CHtml::encode($data->account); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('class_info')); ?>:</b>
	<?php echo CHtml::encode($data->class_info); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pwd')); ?>:</b>
	<?php echo CHtml::encode($data->pwd); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />


</div>